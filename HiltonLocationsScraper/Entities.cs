﻿using System;
using System.Collections.Generic;

namespace HiltonLocationsScraper
{
    public class HotelsResponse
    {
        public List<Hotel> Hotels { get; set; }
        public int? HotelListSize { get; set; }
        public int? MaxHotelListSize { get; set; }
    }

	public class RegionResponse
	{
		public List<RegionResponseRegion> Region { get; set; } 
	}

	public class RegionResponseRegion
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Text { get; set; }
		public List<RegionResponseSubregion> Subregion { get; set; } 
	}

	public class RegionResponseSubregion
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}

    public class Hotel
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string Brand { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Ctyhocn { get; set; }
        public string Status { get; set; }
        public string Pic { get; set; }
        public bool Reservations { get; set; }
        public DateTime? OpenDate { get; set; }

        public HotelBrand HotelBrand
        {
            get { return HiltonLocations.HotelCode2Brand(Brand); }
        }

        public bool? Franchised
        {
            get
            {
                var franchiseCodes = HiltonLocations.GetFranchiseStatus(HotelBrand);

                if (franchiseCodes.Result.ContainsKey(Ctyhocn))
                {
                    if (franchiseCodes.Result[Ctyhocn] == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return null;

                throw new ApplicationException("Unable to determine franchise status.");
            }
        }

        public bool? CorporateOwned
        {
            get
            {
                if (Franchised == null)
                {
                    return Franchised;
                }

                return !Franchised;
            }
        }
    }

    public class Region : Subregion
    {
        public bool CanOmitSubregion { get; set; }
        public List<Subregion> Subregions { get; set; }  
    }

    public class Subregion
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public enum HotelBrand
    {
        // HI
        Hilton = 1,
        // HP
        HamptonInn = 2,
        // GI
        HiltonGardenInn = 3,
        // ES
        EmbassySuites = 4,
        // DT
        DoubleTree = 5,
        // CH
        ConradHotels = 6,
        // HW
        HomewoodSuites = 7,
        // HT
        Home2Suites = 8,
        // WA
        WaldorfAstoria = 9
    }

    public class CsvLocation
    {
        public string Country { get; set; }
        public string Status { get; set; }
        public string Brand { get; set; }
        public string Ctyhocn { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}


