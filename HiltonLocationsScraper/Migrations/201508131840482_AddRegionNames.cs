namespace HiltonLocationsScraper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRegionNames : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hotels", "RegionName", c => c.String());
            AddColumn("dbo.Hotels", "SubRegionName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Hotels", "SubRegionName");
            DropColumn("dbo.Hotels", "RegionName");
        }
    }
}
