namespace HiltonLocationsScraper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Hotels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        Name = c.String(),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Brand = c.String(),
                        Phone = c.String(),
                        Address1 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zip = c.String(),
                        Country = c.String(),
                        Ctyhocn = c.String(),
                        Status = c.String(),
                        Picture = c.String(),
                        Reservations = c.Boolean(nullable: false),
                        OpenDate = c.DateTime(),
                        RegionId = c.Int(nullable: false),
                        SubRegionId = c.Int(),
                        FirstSeen = c.DateTime(nullable: false),
                        LastSeen = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Hotels");
        }
    }
}
