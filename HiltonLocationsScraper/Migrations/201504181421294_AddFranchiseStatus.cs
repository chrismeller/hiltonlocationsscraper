namespace HiltonLocationsScraper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFranchiseStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hotels", "Franchised", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Hotels", "Franchised");
        }
    }
}
