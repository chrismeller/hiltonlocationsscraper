namespace HiltonLocationsScraper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FranchisedNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Hotels", "Franchised", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Hotels", "Franchised", c => c.Boolean(nullable: false));
        }
    }
}
