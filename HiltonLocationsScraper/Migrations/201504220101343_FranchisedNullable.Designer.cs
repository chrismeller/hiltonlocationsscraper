// <auto-generated />
namespace HiltonLocationsScraper.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class FranchisedNullable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FranchisedNullable));
        
        string IMigrationMetadata.Id
        {
            get { return "201504220101343_FranchisedNullable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
