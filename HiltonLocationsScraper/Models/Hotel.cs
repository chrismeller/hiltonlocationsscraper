﻿using System;
using System.Data;

namespace HiltonLocationsScraper.Models
{
    public class Hotel
    {
        public int Id { get; set; }

        public string Url { get; set; }
        public string Name { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Brand { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Ctyhocn { get; set; }
        public string Status { get; set; }
        public string Picture { get; set; }
        public bool Reservations { get; set; }
        public DateTime? OpenDate { get; set; }
        public bool? Franchised { get; set; }

        public int RegionId { get; set; }
        public int? SubRegionId { get; set; }

		public string RegionName { get; set; }
		public string SubRegionName { get; set; }

        public DateTime FirstSeen { get; set; }
        public DateTime LastSeen { get; set; }
    }
}
