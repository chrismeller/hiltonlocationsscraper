﻿using System.Data.Entity;

namespace HiltonLocationsScraper.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Hotel> Hotels { get; set; }
    }
}
