﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;

namespace HiltonLocationsScraper
{
    public class HiltonLocations
    {

        private static readonly Dictionary<HotelBrand, Dictionary<string, int>> _franchiseCache = new Dictionary<HotelBrand, Dictionary<string, int>>();

	    private static readonly Dictionary<HotelBrand, string> _brandEndpoints = new Dictionary<HotelBrand, string>()
	    {
		    {HotelBrand.Hilton, "http://www3.hilton.com/en_US/hi/ajax/cache/"},
		    {HotelBrand.HamptonInn, "http://hamptoninn3.hilton.com/en_US/hp/ajax/cache/"},
			{HotelBrand.HiltonGardenInn, "http://hiltongardeninn3.hilton.com/en_US/gi/ajax/cache/"},
			{HotelBrand.EmbassySuites, "http://embassysuites3.hilton.com/en_US/es/ajax/cache/"},
			{HotelBrand.DoubleTree, "http://doubletree3.hilton.com/en_US/dt/ajax/cache/"},
			{HotelBrand.ConradHotels, "http://conradhotels3.hilton.com/en_US/ch/ajax/cache/"},
			{HotelBrand.HomewoodSuites, "http://homewoodsuites3.hilton.com/en_US/hw/ajax/cache/"},
			{HotelBrand.Home2Suites, "http://home2suites3.hilton.com/en_US/ht/ajax/cache/"},
			{HotelBrand.WaldorfAstoria, "http://waldorfastoria3.hilton.com/en_US/wa/ajax/cache/"}
	    };

		private static readonly List<string> _ignoreRegions = new List<string>()
		{
			"Top Destinations"
		}; 

	    private static async Task<RegionResponse> GetRegions(string endpoint)
	    {
		    var client = new RestClient(endpoint);
			client.ClearHandlers();
			client.AddHandler("text/plain", new JsonDeserializer());

			var request = new RestRequest("regions.json", Method.GET);

		    var response = await client.GetTaskAsync<RegionResponse>(request);

			// filter out any regions we've identified as crap, like "Top Destinations"
		    response.Region = response.Region.Where(x => !_ignoreRegions.Contains(x.Name)).ToList();

		    return response;
	    }

	    public static async Task<List<RegionResponseRegion>> Regions(HotelBrand brand)
	    {
			switch (brand)
			{
				case HotelBrand.Hilton:
					return (await GetRegions(_brandEndpoints[HotelBrand.Hilton])).Region;
					break;
				case HotelBrand.HamptonInn:
					return (await GetRegions(_brandEndpoints[HotelBrand.HamptonInn])).Region;
					break;
				case HotelBrand.HiltonGardenInn:
					return (await GetRegions(_brandEndpoints[HotelBrand.HiltonGardenInn])).Region;
					break;
				case HotelBrand.EmbassySuites:
					return (await GetRegions(_brandEndpoints[HotelBrand.EmbassySuites])).Region;
					break;
				case HotelBrand.DoubleTree:
					return (await GetRegions(_brandEndpoints[HotelBrand.DoubleTree])).Region;
					break;
				case HotelBrand.ConradHotels:
					return (await GetRegions(_brandEndpoints[HotelBrand.ConradHotels])).Region;
					break;
				case HotelBrand.HomewoodSuites:
					return (await GetRegions(_brandEndpoints[HotelBrand.HomewoodSuites])).Region;
					break;
				case HotelBrand.Home2Suites:
					return (await GetRegions(_brandEndpoints[HotelBrand.Home2Suites])).Region;
					break;
				case HotelBrand.WaldorfAstoria:
					return (await GetRegions(_brandEndpoints[HotelBrand.WaldorfAstoria])).Region;
					break;
				default:
					throw new ApplicationException("Invalid Brand Code specified.");
					break;
			}
	    }

        private static async Task<HotelsResponse> GetHotels(string endpoint, int? regionId = null, int? subRegionId = null)
        {
            var client = new RestClient(endpoint);
            client.ClearHandlers();
            client.AddHandler("text/plain", new JsonDeserializer());

            var request = new RestRequest("regionHotels.json", Method.GET);
            request.AddQueryParameter("regionId", regionId.ToString());
            request.AddQueryParameter("subregionId", subRegionId.ToString());
            request.AddQueryParameter("hotelStatus", "null");

            //var response = client.Get<HotelsResponse>(request);
            var response = await client.GetTaskAsync<HotelsResponse>(request);

            return response;
        }

        public static async Task<List<Hotel>> Hilton(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels(_brandEndpoints[HotelBrand.Hilton], regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> Hampton(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels(_brandEndpoints[HotelBrand.HamptonInn], regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> HiltonGardenInn(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels(_brandEndpoints[HotelBrand.HiltonGardenInn], regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> EmbassySuites(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels("http://embassysuites3.hilton.com/en_US/es/ajax/cache/", regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> DoubleTree(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels("http://doubletree3.hilton.com/en_US/dt/ajax/cache/", regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> ConradHotels(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels("http://conradhotels3.hilton.com/en_US/ch/ajax/cache/", regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> HomewoodSuites(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels("http://homewoodsuites3.hilton.com/en_US/hw/ajax/cache/", regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> Home2Suites(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels("http://home2suites3.hilton.com/en_US/ht/ajax/cache/", regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> WaldorfAstoria(int? regionId = null, int? subRegionId = null)
        {
            return (await GetHotels("http://waldorfastoria3.hilton.com/en_US/wa/ajax/cache/", regionId, subRegionId)).Hotels;
        }

        public static async Task<List<Hotel>> Locations(HotelBrand brand, int? regionId = null, int? subRegionId = null)
        {
            switch (brand)
            {
                case HotelBrand.Hilton:
                    return await Hilton(regionId, subRegionId);
                    break;
                case HotelBrand.HamptonInn:
                    return await Hampton(regionId, subRegionId);
                    break;
                case HotelBrand.HiltonGardenInn:
                    return await HiltonGardenInn(regionId, subRegionId);
                    break;
                case HotelBrand.EmbassySuites:
                    return await EmbassySuites(regionId, subRegionId);
                    break;
                case HotelBrand.DoubleTree:
                    return await DoubleTree(regionId, subRegionId);
                    break;
                case HotelBrand.ConradHotels:
                    return await ConradHotels(regionId, subRegionId);
                    break;
                case HotelBrand.HomewoodSuites:
                    return await HomewoodSuites(regionId, subRegionId);
                    break;
                case HotelBrand.Home2Suites:
                    return await Home2Suites(regionId, subRegionId);
                    break;
                case HotelBrand.WaldorfAstoria:
                    return await WaldorfAstoria(regionId, subRegionId);
                    break;
                default:
                    throw new ApplicationException("Invalid Brand Code specified.");
                    break;
            }
        }

        /// <summary>
        /// Gets a list of hotel codes => franchise status. 1 indicates a franchise, 2 means it's company-owned.
        /// </summary>
        /// <param name="brand">The hotel brand to fetch.</param>
        /// <returns></returns>
        public static async Task<Dictionary<string, int>> GetFranchiseStatus(HotelBrand brand)
        {
            
            // do we have it cached?
            if (_franchiseCache.ContainsKey(brand))
            {
                return _franchiseCache[brand];
            }

            var client = new HttpClient
            {
                BaseAddress = new Uri("http://service.maxymiser.net/cdn/hilton/hilton.com/pc_criteria/franchise_codes/")
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-javascript"));

            var filename = String.Format("fcodes_{0}.js", HotelBrand2Code(brand).ToLower());

            HttpResponseMessage response = await client.GetAsync(filename);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();

            // remove the opening and closing JS
            json = json.Replace("mmcore._fCodes=", "");
            json = json.Remove(json.IndexOf("/*"));

            Dictionary<string, int> list = JsonConvert.DeserializeObject<Dictionary<string, int>>(json);

            // save the results in our cache
            _franchiseCache[brand] = list;

            return list;

        }

        public static string HotelBrand2Code(HotelBrand brand)
        {
            switch (brand)
            {
                case HotelBrand.Hilton:
                    return "HI";
                    break;
                case HotelBrand.HamptonInn:
                    return "HP";
                    break;
                case HotelBrand.HiltonGardenInn:
                    return "GI";
                    break;
                case HotelBrand.EmbassySuites:
                    return "ES";
                    break;
                case HotelBrand.DoubleTree:
                    return "DT";
                    break;
                case HotelBrand.ConradHotels:
                    return "CH";
                    break;
                case HotelBrand.HomewoodSuites:
                    return "HW";
                    break;
                case HotelBrand.Home2Suites:
                    return "HT";
                    break;
                case HotelBrand.WaldorfAstoria:
                    return "WA";
                    break;
                default:
                    throw new ApplicationException("Invalid Brand Code specified.");
                    break;
            }
        }

        public static HotelBrand HotelCode2Brand(string code)
        {
            switch (code)
            {
                case "HI":
                    return HotelBrand.Hilton;
                    break;
                case "HP":
                    return HotelBrand.HamptonInn;
                    break;
                case "GI":
                    return HotelBrand.HiltonGardenInn;
                    break;
                case "ES":
                    return HotelBrand.EmbassySuites;
                    break;
                case "DT":
                    return HotelBrand.DoubleTree;
                    break;
                case "CH":
                    return HotelBrand.ConradHotels;
                    break;
                case "HW":
                    return HotelBrand.HomewoodSuites;
                    break;
                case "HT":
                    return HotelBrand.Home2Suites;
                    break;
                case "WA":
                    return HotelBrand.WaldorfAstoria;
                    break;
                default:
                    throw new ApplicationException("Invalid hotel brand!");
                    break;
            }
        }

        public static async Task<IEnumerable<CsvLocation>> GetAllProperties()
        {

            var client = new HttpClient
            {
                BaseAddress = new Uri("http://www.hiltonworldwide.com/development/")
            };

            HttpResponseMessage response = await client.GetAsync("hotel-properties.csv");
            response.EnsureSuccessStatusCode();

            var psv = await response.Content.ReadAsStringAsync();

            var locations = from line in psv.Split(new string[] { "\r\n" }, StringSplitOptions.None)
                // fix a screwup on hilton's part where some hotels are missing a Ctyhocn, which results in an empty extra delimiter somehow
                let line2 = line.Replace("|||", "||")
                let columns = line2.Split('|')
                select new CsvLocation()
                {
                    Country = columns[0],
                    Status = columns[1],
                    Brand = columns[2],
                    Ctyhocn = columns[3],
                    Latitude = Convert.ToDecimal(columns[4]),
                    Longitude = Convert.ToDecimal(columns[5])
                };

            return locations;

        }

    }
}
