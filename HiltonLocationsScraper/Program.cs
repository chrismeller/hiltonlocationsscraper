﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using HiltonLocationsScraper.Models;

namespace HiltonLocationsScraper
{
	internal class Program
	{
		private static void Main(string[] args)
		{

			var locations = HiltonLocations.GetAllProperties().Result;

			var franchises = HiltonLocations.GetFranchiseStatus(HotelBrand.Hilton).Result;


            Console.WriteLine("HAMPTON INN -----");
            GetLocations(HotelBrand.HamptonInn).Wait();

            Console.WriteLine("HILTON -----");
            GetLocations(HotelBrand.Hilton).Wait();

            Console.WriteLine("DOUBLETREE -----");
            GetLocations(HotelBrand.DoubleTree).Wait();

            Console.WriteLine("EMBASSY SUITES -----");
            GetLocations(HotelBrand.EmbassySuites).Wait();

            Console.WriteLine("GARDEN INN -----");
            GetLocations(HotelBrand.HiltonGardenInn).Wait();

            Console.WriteLine("CONRAD HOTELS -----");
            GetLocations(HotelBrand.ConradHotels).Wait();

            Console.WriteLine("HOME2 SUITES -----");
            GetLocations(HotelBrand.Home2Suites).Wait();

            Console.WriteLine("HOMEWOOD SUITES -----");
            GetLocations(HotelBrand.HomewoodSuites).Wait();

            Console.WriteLine("WALDORF ASTORIA -----");
			GetLocations(HotelBrand.WaldorfAstoria).Wait();

			Console.WriteLine("Done");
			//Console.ReadKey();

		}

		public static void SaveLocations(List<Hotel> locations, RegionResponseRegion region, RegionResponseSubregion subRegion = null)
		{

			// we're going to mark all the entries in the database as being last seen as of today to keep track of historical values -- round it to today to account for incomplete runs on the same day
			var lastSeen = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 0, 0, 0, DateTimeKind.Utc);

			using (var db = new ApplicationDbContext())
			{

				if (locations == null)
				{
					Console.WriteLine("NO LOCATIONS FOUND!");
					return;
				}
				else
				{
					Console.Write("\t\tGot {0} items", locations.Count());
				}

				foreach (var location in locations)
				{
					// see if there is an existing record
					var existing = db.Hotels.FirstOrDefault(h => h.Ctyhocn == location.Ctyhocn);

					Models.Hotel hotel;

					if (existing != null)
					{
						hotel = existing;
					}
					else
					{
						hotel = new Models.Hotel();
					}

					// dump the dto object into the entity
					hotel.Url = location.Url;
					hotel.Name = location.Name;
					hotel.Latitude = location.Lat;
					hotel.Longitude = location.Lng;
					hotel.Brand = location.Brand;
					hotel.Phone = location.Phone;
					hotel.Address1 = location.Address1;
					hotel.City = location.City;
					hotel.State = location.State;
					hotel.Zip = location.Zip;
					hotel.Country = location.Country;
					hotel.Ctyhocn = location.Ctyhocn;
					hotel.Status = location.Status;
					hotel.Picture = location.Pic;
					hotel.Reservations = location.Reservations;
					hotel.OpenDate = location.OpenDate;
					hotel.LastSeen = lastSeen;
					hotel.Franchised = location.Franchised;

					hotel.RegionId = region.Id;
					hotel.RegionName = region.Name;

					if (subRegion != null)
					{
						hotel.SubRegionId = subRegion.Id;
						hotel.SubRegionName = subRegion.Name;
					}

					// only include the first seen date if it's a new entry
					if (existing == null)
					{
						hotel.FirstSeen = lastSeen;
					}

					// validate the opendate
					if (hotel.OpenDate.HasValue && hotel.OpenDate.Value.Year < 100)
					{
						hotel.OpenDate = hotel.OpenDate.Value.AddYears(2000);
					}

					// we only need to add the hotel as a new element if this is not an update
					if (existing == null)
					{
						db.Hotels.Add(hotel);

						//Console.WriteLine("Adding Hotel {0}: {1}", hotel.Ctyhocn, hotel.Name);
					}
					else
					{
						db.Hotels.AddOrUpdate(hotel);
						//Console.WriteLine("Updating hotel {0}: {1}", hotel.Ctyhocn, hotel.Name);
					}
				}

				try
				{
					var wrote = db.SaveChanges();

					Console.WriteLine("\tWrote {0} items", wrote);

					if (wrote != locations.Count)
					{


						Console.WriteLine("\t\tCounts don't match! {0} wrote, {1} locations! Press a key.", wrote, locations.Count);
						//Console.ReadKey();
					}
				}
				catch (System.Data.Entity.Validation.DbEntityValidationException e_validation)
				{
					foreach (var error in e_validation.EntityValidationErrors)
					{
						Console.Write("Validation Errors:");
						foreach (var validation_error in error.ValidationErrors)
						{
							Console.WriteLine(validation_error.ErrorMessage);
						}

						Console.WriteLine("Entity:");
						foreach (var key in error.Entry.CurrentValues.PropertyNames)
						{
							Console.WriteLine(key + ": " + error.Entry.CurrentValues[key]);
						}
					}
					throw;
				}
				catch (Exception e)
				{
					throw;
					Console.WriteLine(e.ToString());
				}

			}
		}

		public static async Task GetLocations(HotelBrand brand)
		{
			var regions = await HiltonLocations.Regions(brand);

			foreach (var region in regions)
			{
				Console.WriteLine("{0}: {1}", region.Name, region.Id);

				if (region.Subregion == null)
				{
					var locations = await HiltonLocations.Locations(brand, region.Id);

					SaveLocations(locations, region);
				}
				else
				{

					foreach (var subregion in region.Subregion)
					{
						Console.WriteLine("{0}: {1}", region.Name, subregion.Name);

						var locations = await HiltonLocations.Locations(brand, region.Id, subregion.Id);

						SaveLocations(locations, region, subregion);
					}

				}
			}
		}
	}

}
